// @JUDGE_ID:  1000AA  100  Java  "Easy algorithm"

import java.io.*;
import java.util.*;

class Main
{
    static String ReadLn (int maxLg)  // utility function to read from stdin
    {
        byte lin[] = new byte [maxLg];
        int lg = 0, car = -1;
        String line = "";

        try
        {
            while (lg < maxLg)
            {
                car = System.in.read();
                if ((car < 0) || (car == '\n')) break;
                lin [lg++] += car;
            }
        }
        catch (IOException e)
        {
            return (null);
        }

        if ((car < 0) && (lg == 0)) return (null);  // eof
        return (new String (lin, 0, lg));
    }

    public static void main (String args[])  // entry point from OS
    {
        Main myWork = new Main();  // create a dinamic instance
        myWork.Begin();            // the true entry point
    }

    void Begin()
    {
        String input;
        StringTokenizer idata;
        int[] base;
        int cases,count,start,end,a,b,print = 1;

        while ((input = Main.ReadLn (1000000)) != null)
        {
            System.out.println("Case " + print++ + ":");
            base = new int[input.length()];
            for (int i = 0; i < input.length(); i++) {
                base[i] = Integer.parseInt("" + input.charAt(i));
            }
            
//            System.out.println(Arrays.toString(base));
          
            cases = Integer.parseInt(Main.ReadLn(255));
            
            for (int i = 0; i < cases; i++) {
                count = 0;
                
                input = Main.ReadLn(255);
                idata = new StringTokenizer(input);
                
                a = Integer.parseInt(idata.nextToken());
                b = Integer.parseInt(idata.nextToken());
                
                start = Math.min(a, b);
                end = Math.max(a, b);
                
                for (int j = start; j <= end; j++) {
                    count += base[j];
                }
                
                if (count == 0 || count == end-start+1){
                    System.out.println("Yes");
                } else {
                    System.out.println("No");
                }
            }
          
        }
    }
}