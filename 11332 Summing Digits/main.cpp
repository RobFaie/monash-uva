#include <iostream>

using namespace std;

int main() {
	unsigned long a = 0;
	unsigned short b = 0;

	cin >> a;
	while (a != 0) {
		b = a % 9;
		if (b == 0)
			cout << 9 << '\n';
		else
			cout << b << '\n';
		cin >> a;
	}
}
