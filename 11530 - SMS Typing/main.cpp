#include <iostream>
using namespace std;

int main() {
	unsigned long keys[256];

	keys['a'] = 1;
	keys['b'] = 2;
	keys['c'] = 3;

	keys['d'] = 1;
	keys['e'] = 2;
	keys['f'] = 3;

	keys['g'] = 1;
	keys['h'] = 2;
	keys['i'] = 3;

	keys['j'] = 1;
	keys['k'] = 2;
	keys['l'] = 3;

	keys['m'] = 1;
	keys['n'] = 2;
	keys['o'] = 3;

	keys['p'] = 1;
	keys['q'] = 2;
	keys['r'] = 3;
	keys['s'] = 4;

	keys['t'] = 1;
	keys['u'] = 2;
	keys['v'] = 3;

	keys['w'] = 1;
	keys['x'] = 2;
	keys['y'] = 3;
	keys['z'] = 4;
	
	keys[' '] = 1;

	unsigned long cases, total;
	char c;

	cin >> cases;
	cin.get(c);

	for (int i = 1; i <= cases; ++i) {
		total = 0;
		while(cin.get(c)) {
			if (c == '\n')
				break;
			total += keys[c];
		}
		cout << "Case #" << i << ": " << total << '\n';
	}
}