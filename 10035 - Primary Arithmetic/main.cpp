#include <iostream>

using namespace std;

int main() {
	unsigned short count = 0;
	unsigned short A[10];
	unsigned short B[10];
	unsigned short i,j,k,l;
	char c;

	while(true) {
		i = 0;
		j = 0;
		count = 0;
		cin.get(c);
		while(c != ' ') {
			A[i++] = c - '0';
			cin.get(c);
		}
		cin.get(c);
		while (c != '\n') {
			B[j++] = c - '0';
			cin.get(c);
		}
		if (i == 1 && j == 1) break;
		while (i > 0 && j > 0) {
			if ((A[i-1] + B[j-1]) > 9) {
				count++;
				if (i != 1 && j != 1) {
					if (A[i-2] != 9)
						A[i-2]++;
					if (B[j-2] != 9)
						B[j-2]++;
					else{
						count++;
						A[i-2] = 0;
					}
				}
			}
			--i;
			--j;
		}
		if (count == 0) {
			cout << "No carry operation." << '\n';
		} else if (count == 1) {
			cout << "1 carry operation." << '\n';			
		} else {
			cout << count << " carry operations." << '\n';
		}
	}

}
