// @JUDGE_ID:  1000AA  100  Java  "Easy algorithm"

import java.io.*;
import java.util.*;

class Main
{
    static String ReadLn (int maxLg)  // utility function to read from stdin
    {
        byte lin[] = new byte [maxLg];
        int lg = 0, car = -1;
        String line = "";

        try
        {
            while (lg < maxLg)
            {
                car = System.in.read();
                if ((car < 0) || (car == '\n')) break;
                lin [lg++] += car;
            }
        }
        catch (IOException e)
        {
            return (null);
        }

        if ((car < 0) && (lg == 0)) return (null);  // eof
        return (new String (lin, 0, lg));
    }

    public static void main (String args[])  // entry point from OS
    {
        Main myWork = new Main();  // create a dinamic instance
        myWork.Begin();            // the true entry point
    }
    
    private static int g(long n) {
        long total = 0, start = n;
        while (total >= 10) {
            total = 0;
            
        }
        return 0;
    }

    void Begin()
    {
        String input;
        StringTokenizer idata;
        int i;

        while ((input = Main.ReadLn (255)) != null)
        {
          idata = new StringTokenizer (input);
          i = Integer.parseInt(idata.nextToken());
          if (i == 0)
              break;
            System.out.println(i % 9 != 0 ? i % 9 : 9);
        }
    }
}