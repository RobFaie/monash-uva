#include <iostream>
using namespace std;

unsigned short cycle(unsigned int n) {
	unsigned short c = 1;
	while (n != 1) {
		if (n % 2 == 0) {
			n = n / 2;
		} else {
			n = 3 * n + 1;
		}
		++c;
	}
	return c;
}

int main() {
	unsigned int i,j, t,m,n;
	unsigned short k,l;

	while (cin >> i) {
		cin >> j;

		m = i;
		n = j;

		if (j < i) {
			t = i;
			i = j;
			j = t;
		}

		l = cycle(i);
		for (int m = i+1; m <= j; ++m) {
			k = cycle(m);
			if (k > l) {
				l = k;
			}
		}
		cout << m << ' ' << n << ' ' << l << '\n';
	}
}
