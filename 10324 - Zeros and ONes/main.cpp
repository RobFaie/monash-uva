#include <iostream>
using namespace std;

int main() {
	bool a[1000005];
	bool b,d;
	unsigned long i,j,k,l,z,tmp;
	char c;
	cin.get(c);
	z = 0;
	while (cin && c != '\n') {
		cout << "Case " << ++z << ":\n";
		i = 0;
		while(c != '\n') {
			a[i++] = c == '1';
			cin.get(c);
		}
		cin >> j;
		for (int r = 0; r < j; ++r) {
			cin >> k >> l;
			if (k > l){
				tmp = k;
				k = l;
				l = tmp;
			}
			b = a[k];
			d = true;
			for (int s = k; s <= l; ++s){
				if (a[s] != b) {
					d = false;
					break;
				}
			}
			if (d) {
				cout << "Yes" << '\n';
			} else {
				cout << "No" << '\n';
			}
		}
		cin.get(c);
		cin.get(c);
	}
}
