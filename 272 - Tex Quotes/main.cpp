#include <iostream>
using namespace std;

int main() {
	bool left = true;
	char c;

	while(cin.get(c)) {
		if (c == '\"'){
			if (left)
				cout << "``";
			else
				cout << "''";
			left = !left;
		} else
			cout << c;
	}
}
