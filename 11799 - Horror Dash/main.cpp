#include <iostream>

using namespace std;

int main() {

	unsigned int n, t, max, tmp;

	cin >> t;
	for (int i = 1; i <= t; ++i) {
		max = 0;
		cin >> n;
		for (int j = 0; j < n; ++j) {
			cin >> tmp;
			if (tmp > max)
				max = tmp;
		}

		cout << "Case " << i << ": " << max << '\n';
	}
}
