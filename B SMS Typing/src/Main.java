
// @JUDGE_ID:  1000AA  100  Java  "Easy algorithm"

import java.io.*;
import java.util.*;

class Main
{
    static String ReadLn (int maxLg)  // utility function to read from stdin
    {
        byte lin[] = new byte [maxLg];
        int lg = 0, car = -1;
        String line = "";

        try
        {
            while (lg < maxLg)
            {
                car = System.in.read();
                if ((car < 0) || (car == '\n')) break;
                lin [lg++] += car;
            }
        }
        catch (IOException e)
        {
            return (null);
        }

        if ((car < 0) && (lg == 0)) return (null);  // eof
        return (new String (lin, 0, lg));
    }

    public static void main (String args[])  // entry point from OS
    {
        Main myWork = new Main();  // create a dinamic instance
        myWork.Begin();            // the true entry point
    }

    void Begin()
    {
        String input;
        StringTokenizer idata;
        
        int total, pressTotal;
        char[] line;
        int[] presses = {
            1,2,3,
            1,2,3,
            1,2,3,
            1,2,3,
            1,2,3,
            1,2,3,4,
            1,2,3,
            1,2,3,4
        };
        
        while ((input = Main.ReadLn(255)) != null){
            idata = new StringTokenizer (input);
            total = Integer.parseInt(idata.nextToken());
            for (int i=0; i<total; i++) {
            pressTotal = 0;
                line = Main.ReadLn(255).toCharArray();
                for (int j=0; j<line.length; j++){
                    if(line[j] == ' '){
                        pressTotal += 1;
                    } else {
                        pressTotal += presses[line[j]-97];
                    }
                }
                
                System.out.println("Case #" + (i+1) + ": " + pressTotal);
            }
        }
    }
}