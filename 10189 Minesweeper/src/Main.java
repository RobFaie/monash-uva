
import java.io.*;
import java.util.*;

class Main {

    static String ReadLn(int maxLg) {

        byte lin[] = new byte[maxLg];
        int lg = 0, car = -1;
        String line = "";

        try {
            while (lg < maxLg) {

                car = System.in.read();
                if ((car < 0) || (car == '\n')) {
                    break;
                }
                lin[lg++] += car;
            }
        } catch (IOException e) {
            return (null);
        }

        if ((car < 0) && (lg == 0)) {
            return (null);  // eof
        }
        return (new String(lin, 0, lg));
    }

    public static void main(String args[]) {
        Main myWork = new Main();
        myWork.Begin();
    }

    void Begin() {
        String input, line;
        StringTokenizer idata;
        int a, b, c, d, cols, rows, count = 0;
        int[][] game;

        while ((input = Main.ReadLn(255)) != null) {
            if (input.equals("0 0"))
                break;
            idata = new StringTokenizer(input);

            rows = Integer.parseInt(idata.nextToken());
            cols = Integer.parseInt(idata.nextToken());
            
            game = new int[rows][cols];

            for (int i = 0; i < rows; i++) {
                line = Main.ReadLn(255);
                for (int j = 0; j < cols; j++) {
                    if (line.charAt(j) == '*') {
                        game[i][j] = 9;
                        if (i != 0) {
                            if (j != 0) {
                                game[i - 1][j - 1] += 1;
                            }
                            game[i - 1][j] += 1;
                            if (j != cols - 1) {
                                game[i - 1][j + 1] += 1;
                            }
                        }
                        if (j != 0) {
                            game[i][j - 1] += 1;
                        }
                        if (j != cols - 1) {
                            game[i][j + 1] += 1;
                        }
                        if (i != rows - 1) {
                            if (j != 0) {
                                game[i + 1][j - 1] += 1;
                            }
                            game[i + 1][j] += 1;
                            if (j != cols - 1) {
                                game[i + 1][j + 1] += 1;
                            }
                        }
                    }
                }
            }

            System.out.println("Field #" + ++count + ":");
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    System.out.print(game[i][j] < 9 ? game[i][j] : "*");
                }
                System.out.println();
            }

        }
    }
}