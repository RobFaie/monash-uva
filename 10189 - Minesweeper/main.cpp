#include <iostream>
using namespace std;

int main() {
	int field[10005];
	unsigned short m,n,z;
	long o,p;
	char c;
	z = 1;

	while(cin >> n >> m) {
		if (m == 0 && n == 0) break;
		if (z > 1) cout << '\n';

		cout << "Field #" << z++ << ":\n";
		for (int i = 0; i < n; ++i){
			cin.get(c);
			for (int j = 0; j < m; ++j){
				cin.get(c);
				// cout << '-' << c << '-';
				if(c == '*') {
					field[i*m+j] = -100;
				} else {
					field[i*m+j] = 0;
				}
			}
		}
		for (int i = 0; i < n; ++i){
			for (int j = 0; j < m; ++j){
				if (field[i*m+j] >= 0) {
					continue;
				}

				o = i-1;
				p = j-1;
				if (o >= 0 && o < n && p >= 0 && p < m) {
					++field[o*m+p];
				}
				
				o = i-1;
				p = j;
				if (o >= 0 && o < n && p >= 0 && p < m) {
					++field[o*m+p];
				}
				
				o = i-1;
				p = j+1;
				if (o >= 0 && o < n && p >= 0 && p < m) {
					++field[o*m+p];
				}
				
				o = i;
				p = j-1;
				if (o >= 0 && o < n && p >= 0 && p < m) {
					++field[o*m+p];
				}
				
				o = i;
				p = j+1;
				if (o >= 0 && o < n && p >= 0 && p < m) {
					++field[o*m+p];
				}
				
				o = i+1;
				p = j-1;
				if (o >= 0 && o < n && p >= 0 && p < m) {
					++field[o*m+p];
				}
				
				o = i+1;
				p = j;
				if (o >= 0 && o < n && p >= 0 && p < m) {
					++field[o*m+p];
				}
				
				o = i+1;
				p = j+1;
				if (o >= 0 && o < n && p >= 0 && p < m) {
					++field[o*m+p];
				}
				
			}
		}
		for (int i = 0; i < n; ++i){
			for (int j = 0; j < m; ++j){
				if(field[i*m+j]<0) {
					cout << '*';
				} else {
					cout << field[i*m+j];
				}
			}
			cout << '\n';
		}
	}
}
